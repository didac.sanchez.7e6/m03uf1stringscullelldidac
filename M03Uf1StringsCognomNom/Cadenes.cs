﻿using System;
using System.Net;

namespace M03Uf1StringsCognomNom
{
    class Cadenes
    {
        //Feu un programa que rebi per l’entrada 2 seqüències de caràcters i digui si aquestes són iguals.
        public void SonIguals()
        {
            Console.Clear();
            Console.WriteLine("Una paraula");
            string primera = Console.ReadLine();
            Console.WriteLine("Una altre");
            string segona = Console.ReadLine();
            if (primera == segona)
            {
                Console.WriteLine("Son iguals");
            }
            else
            {

                Console.WriteLine("No son iguals");
            }
            Console.Read();
            Console.Clear();
        }
        //Feu un programa que rebi per l’entrada 2 seqüències de caràcters i digui si aquestes són iguals, sense tenir en compte majúscules i minúscules.
        public void SonIguals2()
        {
            Console.Clear();
            Console.WriteLine("Una paraula");
            string primera = Console.ReadLine();
            Console.WriteLine("Una altre");
            string segona = Console.ReadLine();
            primera = primera.ToLower();
            segona = segona.ToLower();
            if (primera == segona)
            {
                Console.WriteLine("Son iguals");
            }
            else
            {

                Console.WriteLine("No son iguals");
            }
            Console.Read();
            Console.Clear();
        }
        //Feu un programa que rebi per l’entrada 1 String i després una serie indefinida de caràcters per anar traient de l’String, si és que el conté, fins que es rep un 0.
        public void PurgaDeCaracters()
        {
            Console.Clear();
            char trure = '0';
            Console.WriteLine("Una paraula");
            string paraula = Console.ReadLine();
            Console.WriteLine("Lertres que vols treure");
            char[] novaParula = paraula.ToCharArray();
            do
            {
                trure = Convert.ToChar(Console.ReadLine());
                for (int i = 0; i < novaParula.Length; i++)
                {
                    if (trure == novaParula[i])
                    {
                        novaParula[i] = ' ';
                    }
                }
            } while (trure != '0');
            for (int i = 0; i < novaParula.Length; i++)
            {
                if (novaParula[i] != ' ')
                {
                    Console.Write(novaParula[i]);
                }

            }
            Console.ReadLine();
            Console.Clear();
        }
        //Feu un programa que rebi per l’entrada 1 seqüència de caràcters i just després dos caràcters, el primer per substituir de l’String pel segon.
        public void SubstitueixElCaracter()
        {
            Console.Clear();

            Console.WriteLine("Una paraula");
            string paraula = Console.ReadLine();
            char[] novaParula = paraula.ToCharArray();
            Console.WriteLine("Lertre que vol treure");
            char trure = Convert.ToChar(Console.ReadLine());
            Console.WriteLine("Letre per sustuir");
            char posar = Convert.ToChar(Console.ReadLine());
            for (int i = 0; i < novaParula.Length; i++)
            {
                if (novaParula[i] == trure)
                {
                    Console.Write(posar);
                }
                else
                {
                    Console.Write(novaParula[i]);
                }
            }
        }
        //Quan les cèl·lules es divideixen, el seu ADN també es replica. De vegades durant aquest procés es produeixen errors i peces individuals d'ADN es codifiquen amb la informació incorrecta. Si comparem dues cadenes d'ADN i comptem les diferències entre elles podem veure quants errors s'han produït. Això es coneix com la "Distància d'Hamming".
        public void DistanciaDeHamming()
        {
            Console.Clear();
            int distanciaHamming = 0;
            Console.WriteLine("Cadena de ADN");
            string cadenaADNPrimera = Console.ReadLine();
            Console.WriteLine("Cadena de ADN");
            string cadenaADNSegona = Console.ReadLine();
            cadenaADNPrimera = cadenaADNPrimera.ToLower();
            cadenaADNSegona = cadenaADNSegona.ToLower();
            if (cadenaADNPrimera.Length == cadenaADNSegona.Length)
            {
                for (int i = 0; i < cadenaADNPrimera.Length; i++)
                {
                    if (cadenaADNPrimera[i] != 'c' && cadenaADNSegona[i] != 'c' && cadenaADNPrimera[i] != 'a' && cadenaADNSegona[i] != 'a' && cadenaADNPrimera[i] != 'g' && cadenaADNSegona[i] != 'g' && cadenaADNPrimera[i] != 't' && cadenaADNSegona[i] != 't')
                    {
                        Console.WriteLine("Esta malt escrit");
                        break;
                    }
                    if (cadenaADNPrimera[i] != cadenaADNSegona[i])
                    {
                        distanciaHamming++;
                    }
                    if (i == cadenaADNSegona.Length - 1)
                    {
                        Console.WriteLine($"La distancia de Hamming és {distanciaHamming}");
                    }
                }
                
            }
            else
            {
                Console.WriteLine("Entrada no valida");
            }
            Console.ReadLine();
            Console.Clear();
        }
        //Feu una aplicació que donada una seqüència amb només ‘(’ i ‘)’, digueu si els parèntesis tanquen correctament.
        public void Parentesis()
        {
            Console.Clear();
            Console.WriteLine("Escriu la secuensia");
            string parentesis = Console.ReadLine();
            int contadorAvierto = 0, contadorCerado = 0;
            bool flag = true;
            foreach (char parentesisUnico in parentesis)
            {
                if (parentesisUnico == '(')
                {
                    contadorAvierto++;
                }
                else if (parentesisUnico == ')' && contadorAvierto != contadorCerado)
                {
                    contadorCerado++;
                }
                else
                {
                    flag = false;
                    break;
                }
            }
            Console.WriteLine($"És {flag && contadorCerado == contadorAvierto} que esta ben escrit");
            Console.ReadLine();
            Console.Clear();
        }
        //Feu un programa que indiqui si una paraula és un palíndrom o no. Recordeu que una paraula és un palíndrom si es llegeix igual d’esquerra a dreta que de dreta a esquerra.
        public void Palindrom()
        {
            Console.Clear();
            Console.WriteLine("Una paraula");
            string paraula = Console.ReadLine();
            paraula = paraula.ToLower();
            bool flag = true;
            for (int i = 0; i < paraula.Length; i++)
            {
                if (paraula[i] != paraula[paraula.Length -1 -i])
                {
                    flag = false;
                    break;
                }
            }
            Console.WriteLine($"És {flag} que es un palindrom");
            Console.ReadLine();
            Console.Clear();
        }
        //Feu un programa que llegeixi paraules, i que escrigui cadascuna invertint l’ordre dels seus caràcters.
        public void InverteixLesParaules()
        {
            Console.Clear();
            Console.WriteLine("Una parula");
            string parula = Console.ReadLine();
            for (int i = parula.Length-1; i >= 0; i--)
            {
                Console.Write(parula[i]);
            }
            Console.ReadLine();
            Console.Clear();
        }
        //Feu un programa que llegeixi un String i inverteixi l’ordre de les paraules dins del mateix.
        public void InverteixLesParaules2()
        {
            Console.Clear();
            Console.WriteLine("Escriu una frase");
            string frase = Console.ReadLine();
            string[] subFrase = frase.Split(' ');
            for (int i = subFrase.Length-1; i >= 0; i--)
            {
                Console.Write($"{subFrase[i]} ");
            }
            Console.ReadLine();
            Console.Clear();
        }
        //Feu un programa que llegeixi una seqüència de lletres acabada en punt i digui si conté la successió de lletres consecutives ‘h’,‘o’, ‘l’, ‘a’ o no.
        public void HolaAdeu()
        {
            Console.Clear();
            Console.WriteLine("escriu la teva frase sense espais");
            string frase = Console.ReadLine();
            frase = frase.ToLower();
            bool flag = false;
            for (int i = 3; i < frase.Length; i++)
            {
                if (frase[i]=='a'&& frase[i-1]=='l'&& frase[i - 2] == 'o' && frase[i - 3] == 'h')
                {
                    flag = true;
                }
            }
            if (flag) Console.WriteLine("Hola");
            else Console.WriteLine("Adeu");
            Console.ReadLine();
            Console.Clear();
        }
        public void Menu()
        {
            string opcio;
            do
            {
                Console.WriteLine("0.Exit");
                Console.WriteLine("1.Són iguals?");
                Console.WriteLine("2.Són iguals? (2)");
                Console.WriteLine("3.Purga de caràcters");
                Console.WriteLine("4.Substitueix el caràcter");
                Console.WriteLine("5.Distància d’Hamming");
                Console.WriteLine("6.Parèntesis");
                Console.WriteLine("7.Palíndrom");
                Console.WriteLine("8.Inverteix les paraules");
                Console.WriteLine("9.Inverteix les paraules (2)");
                Console.WriteLine("10.Hola i adeu");
                opcio = Console.ReadLine();
                switch (opcio)
                {
                    case "0":
                        break;
                    case "1":
                        SonIguals();
                        break;
                    case "2":
                        SonIguals2();
                        break;
                    case "3":
                        PurgaDeCaracters();
                        break;
                    case "4":
                        SubstitueixElCaracter();
                        break;
                    case "5":
                        DistanciaDeHamming();
                        break;
                    case "6":
                        Parentesis();
                        break;
                    case "7":
                        Palindrom();
                        break;
                    case "8":
                        InverteixLesParaules();
                        break;
                    case "9":
                        InverteixLesParaules2();
                        break;
                    case "10":
                        HolaAdeu();
                        break;

                    default:
                        Console.WriteLine("Opcio incorecte");
                        Console.Clear();
                        break;

                }
            } while (opcio != "0");
        }
        static void Main()
        {
            var menu = new Cadenes();
            menu.Menu();
        }
    }
}
